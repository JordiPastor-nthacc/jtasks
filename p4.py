# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
required vars = [
    VAR_CAMIDX: int, camera index; 
    VAR_XONAR: int, soundcard index; 
    VAR_BOX: str, box name (paired with bpod), eg 'BPOD01'; 
    VAR_REC: int, 0=do not record; 1=record; 
    VAR_BLEN: int (even), block len;
    VAR_DATA: int, 0= do not save using rach util; 1= save data using R util,
    VAR_BNUM: int, block number]

Update_status var = []

# known issues:
    - keep-led-on will malfunction if valvetime > 0.3 s
    - todo: set seed if random trials. 

"""
task_version = '0.1'
stage_number = 4
import user_settings as conf # to use vars: conf.VAR_0
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel
import timeit, numpy as np, time, datetime
from toolsR import SoundR, VideoR
import os
from scipy.signal import firwin, lfilter
import sounddevice as sd
from toolsJ import *

########################### relevant variables for the task ##############################

### complete Trial list
startingSide = int(np.random.choice([0, 1])) # which side to start
startingBlock = bool(np.random.choice([True, False])) # type of block
trial_list = list(block(startingSide, repetitive=startingBlock, bsize=int(conf.VAR_BLEN), bnum=int(conf.VAR_BNUM)))


sessionTimeLength = 4500 # secs
sessionTimeStart = time.time()
sessionTimeEnding = sessionTimeStart+sessionTimeLength

evidences = {
    'f1':(1, 0.5, 0.25, 0, -0.25, -0.5, -1)
}

# stimuli (white-noise)
duration = 1 # stim len (secs). Does not imply playing whole stim each time
band_fs = [2000, 20000] # boundaries for the white noise (Hz)
XonarSamplingR = 192000 # soundcard sampling rate
filter_len = 10000 # filter len
# Next we assign sound vector. Returns np.array. How does this work? -> help(whiteNoiseGen)
soundVec = whiteNoiseGen(1.0, band_fs[0], band_fs[1], duration, FsOut=XonarSamplingR, Fn=filter_len) # using amplitude=1.0, adapt to calib values later.

########################## pre-start, load stuff #########################################

### Calibrations
try:
    ValveLtime, ValveCtime, ValveRtime = getWaterCalib(conf.VAR_BOX) # retrieve water calibratino
except:
    raise NameError('Could not find water calibration of '+str(conf.VAR_BOX)+'. Aborting.')
try:
    LAmp, RAmp = getBroadbandCalib(int(conf.VAR_XONAR)) # retrieve broadband calibration
except:
    raise NameError('Could not find broadband calibration of sounddevice '+str(conf.VAR_XONAR)+'. Aborting.')

### Video
currSessionVid = os.path.expanduser('~/VIDEO_pybpod/'+conf.VAR_BOX+'/')
currSessionName = 'test_'+datetime.datetime.now().strftime('%Y%m%d_%H%M')
if not os.path.exists(os.path.expanduser(currSessionVid)):
    os.makedirs(os.path.expanduser(currSessionVid))
cam = VideoR(int(conf.VAR_CAMIDX), path= currSessionVid) #width='default', height='default',#  fps=60)#, fourcc_mjpg='default', showWindows=True)
camOK = False                      
cam.play()
if int(conf.VAR_REC)>0:
        cam.record()

### Data
if int(conf.VAR_DATA) == 1:
    from toolsR import DataR
    currSessionPath = os.path.expanduser('~/DATA_pybpod/'+conf.VAR_BOX+'/') # VAR_BOX should look like 'BPOD01'
    if not os.path.exists(os.path.expanduser(currSessionPath)):
        os.makedirs(os.path.expanduser(currSessionPath))
    mydata = DataR(path=currSessionPath, name=currSessionName)

### misc
LEDINT = 8 # from 0 to 255
LEDL, LEDC, LEDR = (OutputChannel.PWM1,LEDINT), (OutputChannel.PWM2,LEDINT), (OutputChannel.PWM3,LEDINT)

# Create the sound server 
soundStream = SoundR(sampleRate=XonarSamplingR, deviceOut=int(conf.VAR_XONAR), channelsOut=2) #change device depending on the sound card

def my_softcode_handler(data):
    global start, timeit, soundStream, cam
    print(data)
    if data == 68:
        print("Play")
        soundStream.playSound()
        start = timeit.default_timer()
    elif data == 66:
        soundStream.stopSound()
        print(':::::::::Time to stop:::::::::', timeit.default_timer() - start)
        
def remainingTime():
    return sessionTimeEnding-time.time()

######################### BPOD and States ##################################
START_APP = timeit.default_timer()
my_bpod = Bpod()
my_bpod.register_value('STAGE_NUMBER', stage_number) 
my_bpod.register_value('VECTOR_CHOICE', trial_list) # required for trend plugin [0-left, 1-right]
my_bpod.register_value('left_amp', LAmp)
my_bpod.register_value('right_amp', RAmp)
my_bpod.register_value('left_valve', ValveLtime)
my_bpod.register_value('right_valve', ValveRtime)

my_bpod.softcode_handler_function = my_softcode_handler

for i in range(0,len(trial_list),1):  # Main loop
    coh_to_chose_from = np.array(evidences['f1'])
    curr_coh = select_evidence(trial_list[i], coh_to_chose_from)
    
    if trial_list[i] == 0:
        valvetime = ValveLtime
        rewardValve = (OutputChannel.Valve, 1)
        resp = {EventName.Port1In:'Reward', EventName.Port3In:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDL
    elif trial_list[i] == 1:
        valvetime = ValveRtime
        rewardValve = (OutputChannel.Valve, 3)
        resp = {EventName.Port3In:'Reward', EventName.Port1In:'Punish', EventName.Tup:'Invalid'}
        rewardLED = LEDR
    # time this,
    SL,SR, EL, ER = envelope(curr_coh, soundVec, 1, 20, samplingR=XonarSamplingR, variance=0.06, randomized=False, paired=False, LAmp=LAmp, RAmp=RAmp)
    soundStream.load(SL, SR)
    my_bpod.register_value('coherence01', curr_coh)
    my_bpod.register_value('left_envelope', EL.tolist())
    my_bpod.register_value('right_envelope', ER.tolist())

    sma = StateMachine(my_bpod)
    sma.set_global_timer_legacy(timer_id=1, timer_duration=remainingTime())
    
    sma.add_state(#WaitCPoke
        state_name='WaitCPoke',
        state_timer=1,
        state_change_conditions={Bpod.Events.Port2In:'Fixation', Bpod.Events.GlobalTimer1_End: 'exit'},
        output_actions=[LEDC,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(#Fixation
        state_name='Fixation',
        state_timer=0.3,
        state_change_conditions={Bpod.Events.Port2Out:'Invalid', EventName.Tup:'StartSound'},
        output_actions=[LEDC])
    sma.add_state(#StartSound
        state_name='StartSound',
        state_timer=0.1,
        state_change_conditions={EventName.Port2Out:'WaitResponse'},
        output_actions=[(OutputChannel.SoftCode, 68)])#,(Bpod.OutputChannels.GlobalTimerTrig, 1)])
    sma.add_state(
        state_name='WaitResponse',
        state_timer=8,
        state_change_conditions=resp,
        output_actions=[(OutputChannel.SoftCode, 66)])
    sma.add_state(#Reward
        state_name='Reward',
        state_timer=valvetime,
        state_change_conditions={EventName.Tup: 'keep-led-on'},
        output_actions=[rewardValve, rewardLED])
    sma.add_state(#keep-led-on
        state_name='keep-led-on',
        state_timer=0.3-valvetime,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[rewardLED])
    sma.add_state(#Punish
        state_name='Punish',
        state_timer=2,
        state_change_conditions= {EventName.Tup: 'exit'},
        output_actions=[])
     sma.add_state(
        state_name='Invalid',
        state_timer=0.001,
        state_change_conditions={EventName.Tup: 'exit'},
        output_actions=[])

    my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device
    print("Waiting for poke. Reward: ", 'left' if trial_list[i] == 0 else 'right')
    my_bpod.run_state_machine(sma)  # Run state machine
    # no interactive behav
    #sav section
    if int(conf.VAR_DATA)==1:
        tt = my_bpod.session.current_trial
        mydata.add(tt.export())

    if time.time()>sessionTimeEnding:
        break

my_bpod.close()  # Disconnect Bpod and perform post-run actions

cam.stop()
if int(conf.VAR_DATA) == 1:
    mydata.save()

print('EXECUTION TIME', timeit.default_timer() - START_APP)

import glob, sys
sys.path.append(os.path.expanduser('~/pybpod/'))
from daily_reports import report
report.main(glob.glob('*.csv')[0])