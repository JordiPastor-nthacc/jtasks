# !/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Example adapted from Josh Sanders' original version on Sanworks Bpod repository
"""
from pybpodapi.bpod import Bpod
from pybpodapi.state_machine import StateMachine
from pybpodapi.bpod.hardware.events import EventName
from pybpodapi.bpod.hardware.output_channels import OutputChannel


LED1, LED2, LED3 = (OutputChannel.PWM1, 25),(OutputChannel.PWM2, 25),(OutputChannel.PWM3, 25)
#Flush1, Flush2, Flush3 = [(OutputChannel.PWM1, 25), (OutputChannel.Valve, 1)], [(OutputChannel.PWM2, 25), (OutputChannel.Valve, 2)], [(OutputChannel.PWM3, 25), (OutputChannel.Valve, 3)]
valve1, valve2, valve3 = (OutputChannel.Valve, 1), (OutputChannel.Valve, 2), (OutputChannel.Valve, 3)
waitingPoke_change_cond = {EventName.Port1In: 'Flush1', EventName.Port2In: 'Flush2', EventName.Port3In: 'Flush3', EventName.Tup: 'exit'}


my_bpod = Bpod()
sma = StateMachine(my_bpod)

sma.add_state(
    state_name='waitingPoke',
    state_timer=30,
    state_change_conditions=waitingPoke_change_cond,
    output_actions=[LED1, LED2, LED3])
sma.add_state(
    state_name='Flush1',
    state_timer=60,
    state_change_conditions={EventName.Port1Out: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LED1, valve1])
sma.add_state(
    state_name='Flush2',
    state_timer=60,
    state_change_conditions={EventName.Port2Out: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LED2, valve2])
sma.add_state(
    state_name='Flush3',
    state_timer=60,
    state_change_conditions={EventName.Port3Out: 'waitingPoke', EventName.Tup: 'exit'},
    output_actions= [LED3, valve3])

my_bpod.send_state_machine(sma)
my_bpod.run_state_machine(sma)
print("Current trial info: {0}".format(my_bpod.session.current_trial))
my_bpod.close()